import React, { Component } from "react";
import {
  View,
  WebView,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  PermissionsAndroid
} from "react-native";
import ActionButton from "react-native-action-button";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { captureScreen } from "react-native-view-shot";

export default class StudentGame extends Component {
  constructor(props) {
    super(props);
    this.uri = "";
    this.state = {
      description: "",
      url: "",
      flag: false,
      showPopup: false
    };
  }

  takeScreenShot = () => {
    captureScreen({
      format: "png",
      quality: 0.8
    })
      .then(uri => {
        const debug = false;
        if (debug) {
          PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
            {
              title: "Cool Photo App Camera Permission",
              message:
                "Cool Photo App needs access to your camera " +
                "so you can take awesome pictures.",
              buttonNeutral: "Ask Me Later",
              buttonNegative: "Cancel",
              buttonPositive: "OK"
            }
          ).then(granted => {
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
              //this.props.eAction.saveURI(uri);
              this.uri = uri;
              // Your Save flow
              // CameraRoll.saveToCameraRoll(uri).then(() => {
              //   console.log("saved");
              // });
              this.setState({ showPopup: true });
            }
          });
        } else {
          console.log("hello");
          this.setState({ imageURI: uri });
          this.setState({ showPopup: true });
          //this.uploadImage(uri);
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  renderButton() {
    return (
      <ActionButton
        buttonColor="rgba(231,76,60,1)"
        style={{
          position: "absolute",
          bottom: 0,
          right: 0,
          zIndex: 5
        }}
      >
        <ActionButton.Item
          buttonColor="#9b59b6"
          title="Take screenshot & send feedback"
          onPress={() => {
            // this.setState({ showPopup: true });
            this.takeScreenShot();
            // alert("screenshot");
          }}
        >
          <MaterialCommunityIcons
            name="cellphone-screenshot"
            style={styles.actionButtonIcon}
          />
        </ActionButton.Item>
        <ActionButton.Item
          buttonColor="#3498db"
          title="Logout"
          onPress={() => {
            // this.storeData();
            alert("logout");
          }}
        >
          <MaterialCommunityIcons
            name="logout-variant"
            style={styles.actionButtonIcon}
          />
        </ActionButton.Item>
      </ActionButton>
    );
  }

  renderPopup() {
    if (this.state.showPopup) {
      return (
        <View
          style={{
            width: 500,
            height: 300,
            position: "absolute",
            top: 50,
            left: 150,
            backgroundColor: "white",
            zIndex: 3,
            borderRadius: 10
          }}
        >
          <View
            style={{
              flex: 1.5,
              borderBottomWidth: 2,
              borderBottomColor: "#ecf0f1",
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Text style={{ color: "#34495e", fontWeight: "bold" }}>
              FEEDBACK
            </Text>
          </View>
          <View style={{ flex: 6.5 }}>
            <TextInput
              placeholder={"Write your description"}
              value={this.state.description}
              underlineColorAndroid="transparent"
              onChangeText={description =>
                this.setState({ description: description })
              }
            />
          </View>
          <View style={{ flex: 2, flexDirection: "row" }}>
            <TouchableOpacity
              style={{
                flex: 1,
                backgroundColor: "#bdc3c7",
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center"
              }}
              onPress={() => this.setState({ showPopup: false })}
            >
              <MaterialCommunityIcons
                name="arrow-left"
                style={{ color: "white", fontSize: 20, padding: 30, left: -40 }}
              />
              <TouchableOpacity title="Cancel">
                <Text style={{ color: "white", fontSize: 15, left: -30 }}>
                  Cancel
                </Text>
              </TouchableOpacity>
            </TouchableOpacity>

            <TouchableOpacity
              style={{
                flex: 1,
                backgroundColor: "#e74c3c",
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center"
              }}
              onPress={() => {
                // this.props.eAction.uploadImage(
                //   this.uri,
                //   this.state.description
                // );
                this.setState({ showPopup: false });
              }}
            >
              <TouchableOpacity title="Send">
                <Text style={{ color: "white", fontSize: 15, left: 20 }}>
                  Send
                </Text>
              </TouchableOpacity>
              <MaterialCommunityIcons
                name="arrow-right"
                style={{ color: "white", fontSize: 20, padding: 30, left: 30 }}
              />
            </TouchableOpacity>
          </View>
        </View>
      );
    }

    return null;
  }

  render() {
    return (
      <View style={{ flex: 1 }} collapsable={false}>
        <WebView
          source={{
            uri: "https://racing.starmathsonline.com.au/?isloginapp=1"
          }}
        />
        {this.renderButton()}
        {this.renderPopup()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: "white"
  }
});
